var net = require("net");



class Ex2 {

    server;
    clients;
    lastID;

    serverPort = 10000;

    constructor() {
        this.clients = [];
        this.lastID = 0;
    }

    init() {
        this.server = net.createServer(this.userConnection);
        this.server.listen(this.serverPort);
    }

    userConnection = (socket) => {
        // Create a progressibe ID
        this.lastID++;

        const userId = this.lastID;

        // Register client
        this.clients.push({ id: userId, socket: socket });

        

        // Create event for data receiving
        socket.on("data", (data) => {
            const message = data.toString();
            // filter the other connected user
            const destinatari = this.clients.filter(x => x.id !== userId);
            destinatari.forEach(x => x.socket.write(message));
        });
        
        // user close connection
        socket.on("end", () => {
            // delete from client list
            this.clients = this.clients.filter(x => x.id !== userId);
            // console.log('Disconnected user: ' + userId);
        });
    }

    closeConnection() {
        this.server.close();
    }

}

module.exports = Ex2;

