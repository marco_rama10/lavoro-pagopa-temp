const Ex2 = require('./index');
const Ex2Client = require('./index.client');
const assert = require('assert').strict;


// start server
let ex2 = new Ex2();
ex2.init();

describe('chat server', function() {
    

    it('count messages', function(done) {
        // instance 2 client and send message
        let client1 = new Ex2Client();
        let client2 = new Ex2Client();

        client1.connect();
        client2.connect();


        client1.sendMessage('test');
        
        this.timeout(10000);

        setTimeout(() => {
            assert.equal(client2.receivedData.length, 1);
            client1.closeConnection();
            client2.closeConnection();
            done();
        }, 200);

    });

    it('count messages sender', function(done) {
        // instance 2 client and send message
        let client1 = new Ex2Client();
        let client2 = new Ex2Client();

        client1.connect();
        client2.connect();


        client1.sendMessage('test');
        
        this.timeout(10000);

        setTimeout(() => {
            assert.equal(client1.receivedData.length, 0);
            client1.closeConnection();
            client2.closeConnection();
            done();
        }, 200);

    });

    it('count messages multiple clients', function(done) {
        // instance 2 client and send message
        let client1 = new Ex2Client();
        let client2 = new Ex2Client();
        let client3 = new Ex2Client();

        client1.connect();
        client2.connect();
        client3.connect();


        client1.sendMessage('test');
        
        this.timeout(10000);

        setTimeout(() => {
            assert.equal(client2.receivedData.length + client3.receivedData.length, 2);
            client1.closeConnection();
            client2.closeConnection();
            client3.closeConnection();
            done();
        }, 200);

    });


    it('message content', function(done) {
        // instance 2 client and send message
        let client1 = new Ex2Client();
        let client2 = new Ex2Client();

        client1.connect();
        client2.connect();


        client1.sendMessage('test');
        
        this.timeout(10000);

        setTimeout(() => {
            assert.equal(client2.receivedData[0], 'test');
            client1.closeConnection();
            client2.closeConnection();
            done();
            
        }, 200);

    });

    it('close connection', function() {
        console.log(ex2.clients.length);
        ex2.closeConnection();
    });

    
});
