var net = require('net');


class Ex2Client {

      
    client;
    receivedData = [];
    receivedData_callback; // callback_function

    constructor() {
        this.client = new net.Socket();
    }

    connect() {
        this.client.connect(10000, '127.0.0.1', function() {
            // console.log('Connected');
        });

        this.client.on('data', (data) => {
            this.receivedData.push(data.toString());
        });
        
        this.client.on('close', () => {
            // console.log('Connection closed');
        });
    }

    sendMessage(message) {
        this.client.write(message)
    }

    closeConnection() {
        this.client.destroy();
    }

}

module.exports = Ex2Client;


// let c = new Ex2Client();

// c.connect();

// setTimeout(() => c.sendMessage('ciao'), 2000);