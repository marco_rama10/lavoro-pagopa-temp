
// Write some code that will flatten an array of arbitrarily nested arrays of integers into a flat array of integers. e.g. [[1,2,[3]],4] -> [1,2,3,4]. (provide a link to your solution)

// To resolve this problem i need to implement a recursive function
var returnFlattedArray = (array) => {
    return array.reduce((tot, element) => {
        if (typeof element === 'number') {
            // Simple case
            return tot.concat(element);
        } else if (typeof element === 'object' && element.length) {
            // Recursive call
            return tot.concat(returnFlattedArray(element));
        } else {
            // No insert types are no number
            return tot;
        }
        
    }, []);
}


console.log(returnFlattedArray([1,2,3,4]));
console.log(returnFlattedArray([]));
console.log(returnFlattedArray([1, 2, 3, [4, 5], 6, [[7, 8], 9, [10, [11, 12]]]]));
console.log(returnFlattedArray([1, 2, 3, [4, 5], 6, [[7, 'a'], 9, [10, [11, 12]]]]));