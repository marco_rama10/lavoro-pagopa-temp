"use strict";
const User = require('../index');
var assert = require('assert');
describe('Array', function () {
    describe('#indexOf()', function () {
        const User = new User();
        console.log('---------------');
        console.log(User.return1());
        console.log('---------------');
        it('should return -1 when the value is not present', function () {
            assert.equal([1, 2, 3].indexOf(4), -1);
        });
    });
});
