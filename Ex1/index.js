

class Ex1 {
    constructor() {

    }

    flatArray(array) {
        if(!array) { return []; }
        if(typeof array !== 'object' || !array.length) { return []; }


        return array.reduce((tot, element) => {
            if (typeof element === 'number') {
                // Simple case
                return tot.concat(element);
            } else if (typeof element === 'object' && element.length) {
                // Recursive call
                return tot.concat(this.flatArray(element));
            } else {
                // No insert types are no number
                return tot;
            }
            
        }, []);
    }

}

module.exports = Ex1;