const Ex1 = require('./index');
const assert = require('assert').strict;


let ex1 = new Ex1();

describe('flat array test', function() {
    

    it('Pass undefined', function() {
        assert.deepEqual(ex1.flatArray(), []);
    });

    it('Pass empty array', function() {
        
        assert.deepEqual(ex1.flatArray([]), []);
    });

    it('Pass simple array', function() {
        
        assert.deepEqual(ex1.flatArray(
            [1, 2, 3, 4]),
            [1, 2, 3, 4]);
    });

    it('Pass array with nested arrays (1)', function() {
        
        assert.deepEqual(ex1.flatArray(
            [1, 2, 3, [4, 5], 6, [[7, 8], 9, [10, [11, 12]]]]),
            [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        );
    });

    it('Pass array with nested arrays (2)', function() {
        
        assert.deepEqual(ex1.flatArray(
            [1, [2], 3, 4, 5, 6, [[7], [8, 9], [10, [11, 12]]]]),
            [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        );
    });

    it('Pass array with nested arrays amd char', function() {
        
        assert.deepEqual(ex1.flatArray(
            [1, 2, 3, [4, 5], 6, [[7, 'a'], 9, [10, [11, 12]]]]),
            [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12]
        );
    });

    it('Pass no array argument', function() {
        assert.deepEqual(ex1.flatArray(6), []);
    });

    it('Pass char as argument', function() {
        assert.deepEqual(ex1.flatArray(''), []);
    });
});