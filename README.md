Hello Marco!
I'm Danilo, your hiring manager! First of all thanks for applying for Software Engineer, Strategic Innovation!
Your resume matches what we're looking for. But first I'd like you to answer a few questions and write some code.
Your answers and your code are going to be the base for a live interview. In answering the code questions, please submit code as if you intended to ship it to production. The details matter. Tests are expected, as is well written, simple idiomatic code.
I’d recommend you use whatever language you feel strongest in. It doesn’t have to be one we use - we believe good engineers can be productive in any language.
Other applicants have found gist.github.com useful for sharing solutions. I believe this should take less than 3 hours to complete, but understand you may have other commitments and time constraints. Please let me know (roughly) when we should expect your answers (e.g. “over the weekend”). Let me know if you need more time.
It may take us up to 10 days to review your answers.
Thanks,
Danilo Spinelli 
PagoPA S.p.A.
--- Please answer to all questions below ---
(1) Write some code that will flatten an array of arbitrarily nested arrays of integers into a flat array of integers. e.g. [[1,2,[3]],4] -> [1,2,3,4]. (provide a link to your solution)
(2) Write a very simple chat server that should listen on TCP port 10000 for clients. The chat protocol is very simple, clients connect with "telnet" and write single lines of text. On each new line of text, the server will broadcast that line to all other connected clients. Your program should be fully tested too. (provide a link to your solution)
(3) Suppose we have a single production environment (no staging, dev, or test). How would you design the development and release process of a web application that makes it possible to release the "master" branch at any time? How would you manage incrementally developing multiple features in parallel while frequently releasing to production?
